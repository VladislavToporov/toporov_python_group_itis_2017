from random import shuffle, randint

items = [True, False, False]
file1 = open("keep.txt", "w")
file2 = open("change.txt", "w")
winner1 = winner2 = looser1 = looser2 = 0
N = int(1e6)

for i in range(N):
    shuffle(items)
    x = randint(0, 2)
    if items[x]:
        file1.write("%s iter winnner\n" % i)
        winner1 += 1
    else:
        looser1 += 1

    for j in range(3):
        if not items[j]:
            file2.write("door %s is empty" % j)
            break

    if items[x]:
        file2.write("%s iter looser\n" % i)
        looser2 += 1
    else:
        file2.write("%s iter winner\n" % i)
        winner2 += 1
file1.close()
file2.close()
print(winner1, winner2)
