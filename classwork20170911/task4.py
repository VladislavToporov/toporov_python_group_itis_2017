from abc import abstractmethod
class  Car:
    def __init__(self, name, owner):
        self.name = name
        self.owner = owner

    def __add__(self, other):
        return Car(self.name + other.name)

class TeslaCar:
    def __init__(self, name, owner):
        super().__init__(self, name, owner)
        self.electricity = True
    @staticmethod
    def a():
        print("d")
    @abstractmethod
    def b(self):
        pass


TeslaCar.a()
