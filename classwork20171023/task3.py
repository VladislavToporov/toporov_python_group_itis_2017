import pygame, time
from math import sqrt
from pygame.rect import Rect
from classwork20171023.Enemy import Enemy
from classwork20171023.Spaceship import Spaceship

pygame.display.init()
M, N = 800, 400
screen = pygame.display.set_mode((M, N))
pygame.display.set_caption('test')

go = True
change_size_time = time.time()
change_bulet_time = time.time()
change_enemy_time = time.time()

change_enemy2_time = time.time()
ship = Spaceship()
bullets = []
enemies = []
while (go):
    screen.fill((0, 0, 0))
    screen.lock()

    for bullet in bullets:
        for enemy in enemies:
            Spaceship.сollision_detection(bullet, enemy)
    pygame.draw.circle(screen, ship.color, (ship.x0, ship.y0), ship.r, ship.width)
    pygame.draw.circle(screen, (255, 0, 255), (ship.pointer_x, ship.pointer_y), 5, 0)

    for elem in bullets:
        pygame.draw.circle(screen, elem.color, (elem.x0, elem.y0), elem.r, elem.width)

    for elem in enemies:
        pygame.draw.circle(screen, elem.color, (elem.x0, elem.y0), elem.r, elem.width)
    # pygame.draw.rect(screen, (255, 255, 0), (x0, y0, x0 + 100, y0 + 100), 0)
    screen.unlock()
    pygame.display.flip()

    pygame.event.pump()

    if (pygame.key.get_pressed()[pygame.K_ESCAPE]):
        go = False
    if (time.time() - change_size_time > 0.01):
        change_size_time = time.time()
        for elem in bullets:
            elem.y0 -= 1


        if (pygame.key.get_pressed()[pygame.K_LEFT]):
            # ship.x0 -= 1
            ship.move(-1)


        if (pygame.key.get_pressed()[pygame.K_RIGHT]):
            # ship.x0 += 1
            ship.move(1)

        if (pygame.key.get_pressed()[pygame.K_a]):
            ship.rotate(-1)

        if (pygame.key.get_pressed()[pygame.K_d]):
            ship.rotate(1)

    if (time.time() - change_enemy2_time > 0.1):
        change_enemy2_time = time.time()
        for elem in enemies:
            elem.y0 += 1

    if (time.time() - change_bulet_time > 0.5):
        change_bulet_time = time.time()
        if (pygame.key.get_pressed()[pygame.K_SPACE]):
            bullets.append(ship.shoot())


    if (time.time() - change_enemy_time > 2):
        change_enemy_time = time.time()
        enemies.append(Enemy())
pygame.display.quit()
