from math import *
import random


class Enemy:
    def __init__(self) -> None:
        M, N = 800, 400
        self.r = 20
        self.x0 = random.randint(self.r, M - self.r)
        self.y0 = 20
        self.color = (0, 255, 255)
        self.width = 0
