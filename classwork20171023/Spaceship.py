from math import *

from classwork20171023.Bullet import Bullet


class Spaceship:
    def __init__(self) -> None:
        self.r = 50
        self.x0 = 400
        self.y0 = 350
        self.pointer_x = self.x0
        self.pointer_y = self.y0 - self.r
        self.color = (255, 255, 0)
        self.width = 0

    @staticmethod
    def сollision_detection(bullet, target):
        if sqrt((bullet.x0 - target.x0) ** 2 + (bullet.y0 - target.y0) ** 2) <= target.r:
            target.x0 = 1000
            bullet.x0 = 1000

    def move(self, param):
        self.x0 += param
        self.pointer_x += param

    def shoot(self):
        bullet = Bullet(self.pointer_x, self.pointer_y)
        return bullet

    def rotate(self, param):
        a = 0.0175
        dx = round(cos(a))
        dy = round(cos(a))
        if param == -1:
            self.pointer_x -= dx
            self.pointer_y -= dy

        if param == 1:
            self.pointer_x += dx
            self.pointer_y += dy
