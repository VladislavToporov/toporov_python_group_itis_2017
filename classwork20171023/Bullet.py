from math import *
class Bullet:
    def __init__(self, pointer_x, pointer_y) -> None:
        self.r = 5
        self.x0 = pointer_x
        self.y0 = pointer_y
        self.color = (255, 0, 255)
        self.width = 0
