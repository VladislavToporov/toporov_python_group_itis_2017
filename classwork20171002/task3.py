import os
from PIL import Image
while True:
    req = input("")
    if req == "exit":
        break
    command, *param, filename = req.split( )
    if (command == "image" and param[0] == "-scale"
        and param[1].isdigit() and int(param[1]) > 0 and os.path.isfile(os.path.abspath(filename))):

            im = None
            try:
                im = Image.open(filename)
            except:
                print("not image")
                continue
            size = (int(param[1]) / 100 * im.width, int(param[1]) / 100 * im.height)
            file, ext = os.path.splitext(filename)
            im.thumbnail(size)
            os.remove(os.path.abspath(filename))
            im.save(filename, "JPEG")
            print("OK")
    else:
        print("wrong command")
