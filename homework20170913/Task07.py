import re

# generator expressions
lst = (re.sub(r'[(){}\[\],.:;!]', '', s.lower()) for string in open("input07.txt").readlines() for s in string.split())
dic = {}
for letter in lst:
    dic[letter] = dic.get(letter, 0) + 1
print(dic)
