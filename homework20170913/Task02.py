def RtoA(filename):
    lst = (s for string in open(filename).readlines() for s in string.split())
    output = open("output.txt", 'w')

    example = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}

    for number in lst:
        ans = 0
        k = 0
        prev = 'M'
        while k < len(number):

            if example[number[k]] > example[prev]:
                ans -= example[prev] * 2

            ans += example[number[k]]
            prev = number[k]
            k += 1

        output.write(str(ans) + " ")
    output.close()


if __name__ == "__main__":
    RtoA("input2.txt")
