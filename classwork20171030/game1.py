import socket

from classwork20171030.Player import Player

HOST = ''
PORT = 1234
s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s1.bind((HOST, PORT))
s1.listen(2)
flag1 = False
flag2 = False
# conn1, addr1 = None, None
# conn2, addr2 = None, None

conn1, addr1 = s1.accept()
conn2, addr2 = s1.accept()

data1 = conn1.recv(5)
nick1 = data1[0]

data2 = conn2.recv(5)
nick2 = data2[0]

player1 = Player(30, nick1)
player2 = Player(30, nick2)
k = 0
while True:
    if (k % 2 == 0):
        conn1.sendall(bytes(True))
        conn2.sendall(bytes(False))

        data = conn1.recv(1)
        hp1 = player1.hit(data)
        conn1.sendall(bytes([player1.hp, player2.hp]))
        conn2.sendall(bytes([player2.hp, player1.hp]))
        if (int(hp1) <= 0):
            break

    else:
        conn1.sendall(bytes(False))
        conn2.sendall(bytes(True))

        data = conn2.recv(1)
        hp2 = player1.hit(data)
        conn1.sendall(bytes([player1.hp, player2.hp]))
        conn2.sendall(bytes([player2.hp, player1.hp]))
        if (hp2 <= 0):
            break
    k += 1
