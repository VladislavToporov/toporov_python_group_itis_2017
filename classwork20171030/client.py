import socket

HOST = 'localhost'
PORT = 1234
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

message = b"H"
s.sendall(message)
data = s.recv(len(message))
print(str(data))
s.close()
