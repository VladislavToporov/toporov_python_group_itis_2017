import random


class Player:
    def __init__(self, hp, nick):
        self.hp = int(hp)

        self.nick = nick

    def hit(self, power):
        try:
            power = int(power)
        except ValueError:
            return 'Incorrect power'
        if power < 1:
            power = 1
        elif power > 9:
            power = 9
        else:
            if random.randint(1, power).__eq__(power):
                self.hp -= power
                return self.hp
            else:
                return self.hp
