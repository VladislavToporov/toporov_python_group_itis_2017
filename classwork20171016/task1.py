import random
import time


def generate_population(k, n) -> list:
    lst = []
    for i in range(k):
        elem = ''
        for j in range(n):
            elem += chr(random.randint(ord('a'), ord('z')))
        lst.append(elem)
    return lst


def swap(el1, el2) -> tuple:
    elem1 = list(el1)
    elem2 = list(el2)
    for i in range(len(elem1) // 2 - 1):
        if elem1[i] != name[i] and elem2[i] != name[i]: # игра быки и коровы
            elem1[i], elem2[i + 1] = elem2[i + 1], elem1[i]
            elem2[i], elem1[i + 1] = elem1[i + 1], elem2[i]

    el1 = ''.join(elem1)
    el2 = ''.join(elem2)
    return el1, el2


def crossingover(population) -> list:
    for i in range(0, len(population) - 1, 2):
        a, b = swap(population[i], population[i + 1])
        population[i] = a
        population[i + 1] = b
    return population


def fitness(param) -> int:
    sum = 0
    for i in range(len(name)):
        sum += abs(ord(param[i]) - ord(name[i]))
    return sum


def mutation(population) -> list:
    for i in range(len(population)):
        variant = list(population[i])
        mask = [0] * len(name)
        for j in range(len(mask)):
            if name[j] == variant[j]:
                mask[j] = "1"

        for j in range(len(mask)):
            if mask[j] != "1" and random.random() < mutation_probability: # игра быки и коровы
                variant[j] = chr(random.randint(ord('a'), ord('z')))
        population[i] = ''.join(variant)
    return population


def fitness2(variant):
    bull = 0
    cow = 0
    mask = [0] * len(name)
    nice_letters = set()
    for i in range(len(name)):
        if name[i] == variant[i]:
            bull += 1
            mask[i] = 1
        else:
            nice_letters.add(variant[i])
    for i in range(len(name)):
        if mask[i] == "1" and variant[i] in nice_letters:
            cow += 1
    return bull, cow


def go(population, mutation_probability, selection_part):
    counter = 0
    while True:
        counter += 1
        if counter % 10 == 0:
            population.extend(generate_population(len_population // 100, len(name))) # новый генофонд каждые 10 поколений
        if counter % 25 == 0 and mutation_probability < 0.5:
            mutation_probability += 0.05
        if len(population) >= 2:
            population = sorted(population, key=fitness)[: int(len(population) * selection_part)]  # селекция
            if fitness(population[0]) == 0:
                return ("found: " +  str(population[0]) + " | " + str(counter))
            print("crossing: ", crossingover(population))  # скрещивание
            print("mutation: ", mutation(population))  # мутация
            print()
        else:
            return ("fail: " + str(population[0]) + " | " +  str(counter))

name = ""
len_population = 10000
mutation_probability = 0.2
selection_part = 0.8

if __name__ == '__main__':
    name = 'vladislav'
    # name = input().lower()
    population = generate_population(len_population, len(name))
    #print(population)
    #print()
    print(go(population, mutation_probability, selection_part))
