import random

import classwork20171016
from classwork20171016.task1 import *

file = open("stat.txt", 'w')
n = 4
for i in range(n, 8):
    list_of_names = []
    for j in range (50):
        name = ""
        for k in range(n):
            name += chr(random.randint(ord('a'), ord('z')))
        list_of_names.append(name)

    sum = 0
    for j in range(50):
        name = list_of_names[j]
        classwork20171016.task1.name = name
        population = generate_population(len_population, len(name))
        res = go(population, mutation_probability, selection_part)
        sum += int(res.split(" | ")[1])
        file.write(res)
    file.write(str(n) + ": " + str(sum // 50) + "\n")
    file.flush()
    n += 1
file.close()


