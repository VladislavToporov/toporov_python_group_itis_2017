import random
import multiprocessing

import time

from multiprocessing import freeze_support


def printNum1(n, name, lst):
    for i in range (n):
        r = random.randint(0, 100)
        lst[i] = r
        print(r, name)

if __name__ == '__main__':
    lst = multiprocessing.Array("i", 10)
    freeze_support()
    t1 = multiprocessing.Process(target=printNum1, args=(10, "th1", lst))
    #t2 = multiprocessing.Process(target=printNum1, args=(10, "th2"))

    d = time.time()
    t1.start()
    #t2.start()
    t1.join()
    #t2.join()
    print(time.time() - d)
    print(lst[:])


