import random
import multiprocessing

import time

from multiprocessing import freeze_support


def printNum1(n, name, q):
    for i in range (n):
        r = random.randint(0, 100)
        q.put(r)
        print(r, name)

if __name__ == '__main__':
    q = multiprocessing.Queue()
    freeze_support()
    t1 = multiprocessing.Process(target=printNum1, args=(10, "th1", q))
    #t2 = multiprocessing.Process(target=printNum1, args=(10, "th2"))

    d = time.time()
    t1.start()
    t1.join()
    #t2.start()
    #t2.join()
    print(time.time() - d)
    while q.empty() is False:
        print("get", q.get())


