import multiprocessing


def f(x):
    s = 0
    for i in range(1000):
        s += x * x
    print(s)


if __name__ == '__main__':
    multiprocessing.freeze_support()

    p = multiprocessing.Pool()
    p.map(f, [1, 2, 3, 4, 5])
