"""yandexmusic URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView

from music.views import querry, get_tracks, do_login

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r"^music/", include('music.urls')),
    url(r"querry/", querry),
    url(r"artists/(?P<name>([0-9]+))/", get_tracks),
    url(r"index/", TemplateView.as_view(template_name="music/index.html")),
    url(r"login/", do_login, name='login'),


]
