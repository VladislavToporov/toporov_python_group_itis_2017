from django.contrib import admin

# Register your models here.
from music.models import *

admin.site.register(City)
admin.site.register(Country)
admin.site.register(Playlist)
admin.site.register(Artist)
admin.site.register(Audio)