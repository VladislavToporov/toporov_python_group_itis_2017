from django import template


#
# def cut(value, arg):
#     return value.replace(arg, '')
#
#
# def lower(value):
#     return value.lower()
#
# register = template.Library
# register.filter('cut', cut)
# register.filter('lower', lower)
#
#
# # register.simple_tag(lambda x: x - 1, name='minusone')
#
# # @register.simple_tag(name='minustwo')
# # def some_function(value):
# #     return value - 2
#
# @register.simple_tag
# def my_tag(a, b, *args, **kwargs):
#     warning = kwargs['warning']
#     profile = kwargs['profile']
#     return str(a + b)
#
#
