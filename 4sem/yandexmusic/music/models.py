from django.db import models
from django.contrib.auth.models import User
# Create your models here.



class Artist(models.Model):
    name = models.CharField(max_length=50)
    city = models.ForeignKey(to="City", on_delete=models.CASCADE, related_name="artists")
    year_start = models.PositiveSmallIntegerField(null=True, blank=True)
    year_end = models.PositiveSmallIntegerField(null=True, blank=True)
    def __str__(self):
        return self.name

    class Meta:
        verbose_name="исполнитель"
        verbose_name_plural="исполнители"


class Audio(models.Model):
    title = models.CharField(max_length=50)
    artist = models.ForeignKey(to="Artist", on_delete=models.CASCADE, related_name="audios")
    year = models.PositiveSmallIntegerField(null=True, blank=True)
    duration = models.DurationField(null=True, blank=True)

    def __str__(self):
        return self.title


    class Meta:
        verbose_name = "трек"
        verbose_name_plural = "треки"

class Country(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


    class Meta:
        verbose_name = "страна"
        verbose_name_plural = "страны"

class City(models.Model):
    name = models.CharField(max_length=30)
    country = models.ForeignKey(to="Country", on_delete=models.CASCADE, related_name="cities")

    def __str__(self):
        return self.name


    class Meta:
        verbose_name = "город"
        verbose_name_plural = "города"


class Playlist(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    pl_name = models.CharField(max_length=50)
    audios = models.ManyToManyField(to="Audio", related_name='playlists')
    liked = models.ManyToManyField(to=User, through="Like", related_name="liked")

    def __str__(self):
        return self.pl_name


    class Meta:
        verbose_name = "плейлист"
        verbose_name_plural = "плейлисты"


class Like(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name="like_user")
    playlist = models.ForeignKey(to="Playlist", null=True, default=None, on_delete=models.CASCADE, related_name="like_playlist")
    audio = models.ForeignKey(to=Audio, on_delete=models.CASCADE, null=True, related_name="like_playlist")
    when = models.DateTimeField(auto_now=True)


class History(models.Model):
    HISTORY_CHOICES = (
        ('0', 'audios'),
    )
    type = models.CharField(max_length=1, choices=HISTORY_CHOICES)
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name="history_users")
    audio = models.ForeignKey(to=Audio, on_delete=models.CASCADE, related_name="history_audio")
    date = models.DateTimeField(auto_now=True)






