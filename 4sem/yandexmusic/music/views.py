from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q, F
from django.forms import modelform_factory, PasswordInput
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, ListView, FormView, CreateView

from music.forms import ArtistForm, LoginForm, AudioForm
from music.models import *


# Create your views here.


def querry(request):
    # q = Country.objects.all()
    # return HttpResponse(list(q.values_list("name")))


    # tmp = City.objects.get(name="London")
    # q = Artist.objects.filter(city_id = tmp.id)
    # q = Artist.objects.filter(year_start__gt= 1970)

    # c = City.objects.get(name="Liverpool")
    # q = c.artists

    # q = Artist.objects.filter().raw("select * from music_artist where year_start > 1980")

    # q = Artist.objects.filter(year_start__gte=1980)
    # q = q.exclude(city="Los Angeles")



    # q1 = Q(city__name ="Moscow")
    # q2 = Q(year_start__lt=1980)
    # q3 = q1 and not q2

    # q = Playlist.objects.get(pl_name="admin_playlist").audios
    # q = Audio.objects.get(id=1).playlist_set


    q = Artist.objects.filter(year_end__gte=20 + F("year_start"))

    return HttpResponse((q.values("name")))
    # q = Artist.objects.filter(city__name="Liverpool")





    # return HttpResponse(list(q.values_list()))


def get_tracks(request, name):
    print(name)
    lst = Audio.objects.filter(artist__id=name)
    print(lst)
    context = {"tracks": list(lst.values_list("title"))}
    # context = {"tracks": Artist.objects.get(id=name).audios}
    print(context)
    return render(request, "music/tracks.html", context)


class HelloView(TemplateView):
    template_name = "music/index.html"


class TracksView(ListView):
    template_name = 'music/list_of_tracks.html'
    model = Audio
    context_object_name = 'tracks'



class ListArtistsView(ListView):
    template_name = 'music/list_of_artists.html'
    model = Artist
    context_object_name = 'artists'


class ListHistoryView(ListView):
    template_name = 'music/history.html'
    model = History
    paginate_by = 15
    context_object_name = 'history'

    def get_queryset(self):
        return History.objects.filter(user=self.request.user).order_by("-date")


@login_required(login_url=reverse_lazy("login"))
def add_new_artist(request):
    if request.method == "POST":
        form = ArtistForm(data=request.POST)
        form.save()
        return HttpResponseRedirect(reverse('artists'))
    else:
        return render(request, 'music/add_new_artist.html', {"f": ArtistForm()}) #instance= объект


def do_login(request):
    if request.user.is_active:
        return HttpResponseRedirect(reverse('artists'))
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if 'next' in request.GET:
                    return HttpResponseRedirect(request.GET['next'])
                return HttpResponseRedirect(reverse('artists'))
            else:
                messages.add_message(request, messages.INFO, "disabled account")
                return HttpResponseRedirect(reverse('login'))
        else:
            messages.add_message(request, messages.INFO, "wrong login or password")
            return HttpResponseRedirect(reverse('login'))

    else:
        # return render(request, 'music/login.html',
        #               {'form': modelform_factory(model=User, fields=['username', 'password'])})
        return render(request, 'music/login.html',
                      {'form': LoginForm()})


@method_decorator(login_required, name='dispatch')
class AddNewArtistView(CreateView):
    template_name = 'music/add_new_artist.html'
    form_class = ArtistForm
    success_url = reverse_lazy("artists")
    @login_required
    def dispatch(self, request, *args, **kwargs):
        if (request.user.has_perm("wall.add_post")):
            return super(AddNewArtistView, self).dispatch(request, *args, **kwargs)
        else: return HttpResponseRedirect(reverse("artists"))

@method_decorator(login_required, name='dispatch')
class AddNewAudioView(CreateView):
    template_name = 'music/add_new_audio.html'
    form_class = AudioForm
    success_url = reverse_lazy("tracks")
    # @login_required
    # def dispatch(self, request, *args, **kwargs):
    #     return super(AddNewArtistView, self).dispatch(request, *args, **kwargs)

@login_required(login_url=reverse_lazy("login"))
def savetrack(request):
    if request.method == "POST":
        id = request.POST["id"]
        user = request.user
        try:
            playlist = Playlist.objects.get(user=user)
        except Playlist.DoesNotExist:
            playlist = Playlist(pl_name='Мой плейлист' + user.username, user=request.user)
            playlist.save()
            print(playlist.audios)
        audio = Audio.objects.get(id=id)
        playlist.audios.add(audio)
        playlist.save()
        history = History(user=request.user, audio=audio, type='audio')
        history.save()
    return HttpResponseRedirect(reverse("tracks"))

@login_required(login_url=reverse_lazy("login"))
def ajax_like(request, id):
    if Like.objects.filter(user_id=int(request.user.id), audio_id=int(id)).first() == None:
        like = Like()
        like.audio_id = int(id)
        like.user_id = id= int(request.user.id)
        like.save()
    counter = Like.objects.filter(audio_id=id).count()
    return JsonResponse({"counter": counter})


def show_more(request):
    audio = Audio.objects.last()
    print(audio.title)
    return JsonResponse({"track": audio.title})