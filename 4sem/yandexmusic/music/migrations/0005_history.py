# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-03-19 13:36
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('music', '0004_auto_20180312_1644'),
    ]

    operations = [
        migrations.CreateModel(
            name='History',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student_type', models.CharField(choices=[('0', 'audios')], max_length=1)),
                ('date', models.DateTimeField(auto_now=True)),
                ('audio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='audios', to='music.Audio')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='history_users', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
