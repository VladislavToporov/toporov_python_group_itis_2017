from django.conf.urls import url
#
from django.urls import path

from music.views import *

urlpatterns = [
    url(r'^$', HelloView.as_view()),
    url(r'^tracks/', TracksView.as_view(), name='tracks'),
    url(r'^artists/add/', AddNewArtistView.as_view(), name="add_new_artist"),
    url(r'^audio/add/', AddNewAudioView.as_view(), name="add_new_audio"),
    url(r'^artists/', ListArtistsView.as_view(), name='artists'),
    url(r'^savetrack/', savetrack, name='savetrack'),
    url(r'^history/', ListHistoryView.as_view(), name='history'),
    path('ajax/like/<int:id>', ajax_like, name='ajax_like'),
    url('^ajax/', show_more, name='show_more'),
    path('draggable', TemplateView.as_view(template_name="music/draggable.html")),

]
