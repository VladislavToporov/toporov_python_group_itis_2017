from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm, modelform_factory, PasswordInput

from music.models import Artist, Audio


class ArtistForm(ModelForm):
    year_start = forms.IntegerField(required=False)
    year_end = forms.IntegerField(required=False)
    class Meta:
        model = Artist
        exclude = []

class AudioForm(ModelForm):
    year = forms.IntegerField(required=False)
    duration = forms.DurationField(required=False)
    class Meta:
        model = Audio
        exclude = []

LoginForm = modelform_factory(
    model=User,
    fields=['username', 'password'],
    widgets={'password': PasswordInput()}
)