from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse
from django.views.generic import ListView

from wall.forms import PostForm
from wall.models import Post, Repost

class PostView(ListView):
    template_name = 'wall/posts.html'
    model = Post
    context_object_name = 'posts'

    def get_queryset(self):
        return Post.objects.filter(user=self.request.user).order_by("-date")


def make_repost(request, post_id):
    if request.method == "POST":
        origin = Post.objects.get(id=post_id)
        new_post = Post(user=request.user, text="RT %s" % origin.text)
        new_post.save()

        repost = Repost(sourse=origin, destination=new_post, origin_text=origin.text)
        repost.save()
        return HttpResponseRedirect(reverse("posts"))


def edit_repost(request, post_id):
    origin = Post.objects.get(id=post_id)
    if request.method == "POST":
        form = PostForm(data=request.POST, instance=Post.objects.get(id=post_id))
        form.save()
        return HttpResponseRedirect(reverse("posts"))
    else:
        return render(request, "wall/edit.html", {"form": PostForm(instance=origin)}) #instance= объект)

def smth(request):
    if (request.user.has_perm("wall.add_post")):
        pass
    else: return HttpResponseRedirect(reverse("posts"))
