from django import template

register = template.Library()


@register.filter(name='hashtag')
def hashtag(value):
    words = value.split()
    for i in range(len(words)):
        if words[i].startswith("#"):
            words[i] = ' <a href="%s">%s</a>' % (words[i][1:], words[i])
    print(" ".join(words))
    return " ".join(words)
