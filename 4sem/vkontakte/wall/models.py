from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Post(models.Model):
    user = models.ForeignKey(to=User, null=True, on_delete=models.CASCADE, related_name="posts")
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)


class Repost(models.Model):
    sourse = models.ForeignKey(to=Post, related_name="reposts", on_delete=models.CASCADE)
    destination = models.OneToOneField(to=Post, on_delete=models.CASCADE, related_name="src")
    origin_text = models.TextField()

