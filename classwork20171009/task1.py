from datetime import datetime, timedelta
from time import time


def a(filename):
    import re
    string = (s for s in open(filename))
    string = ' '.join(string)
    result = re.finditer(r"(?P<time>([0-1][0-9]:[0-5][0-9]:[0-5][0-9]|2[0-3]:[0-5][0-9]:[0-5][0-9]))", string)
    return (result)


def b(filename):
    result = a(filename)
    delta = timedelta(hours=0, minutes=0, seconds=0)
    for str in result:
        t = datetime.strptime(str.group("time"), "%H:%M:%S")
        delta += timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)
    return (delta)


for elem in a("file1.txt"):
    print(elem.group("time"))
lst = [elem.group("time") for elem in a("file1.txt")]
print(lst)
print(b("file1.txt"))
