import unittest

from classwork20171204.our_strings import is_prefix


class TestOurString(unittest.TestCase):
    def test_is_prefix(self):
        s1 = "N"
        s2 = "Ni"
        self.assertTrue(is_prefix(s1, s2))


if __name__ == '__main__':
    unittest.main()
