def is_prefix(s1, s2):
    """
    :param s1: 
    :param s2: 
    :return: true if s1 is prefix of s2, false otherwise 
    """

    res = True

    for i in range(min(len(s1), len(s2))):
        if s1[i] != s2[i]:
            res = False
            break

        """
        for i, j in s1, s2:
            if i != j:
                return False
        """

    return True
